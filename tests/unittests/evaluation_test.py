from collections.abc import Sequence
from functools import partial
from operator import attrgetter

from jellyfish import jaro_winkler_similarity

from findanywhere.adapters.evaluation.string_distance import evaluate_by_similarity
from findanywhere.adapters.source.text import TextPosition
from findanywhere.ports.evaluation import InputData, EvaluationAdapter, Evaluation
from findanywhere.ports.source import Entry


def test_string_distance_evaluation(input_data: InputData[str], entries: Sequence[Entry[TextPosition, str]]):
    adapter: EvaluationAdapter[TextPosition, str] = partial(evaluate_by_similarity, jaro_winkler_similarity, max)
    evaluation: Evaluation[TextPosition, str] = adapter(input_data, entries)
    assert len(evaluation.best_matches) == len(input_data.fields)
    assert set(map(attrgetter('similarity'), evaluation.best_matches.values())) == {1.0}