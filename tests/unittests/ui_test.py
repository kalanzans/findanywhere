import sys
from pathlib import Path
from secrets import token_urlsafe
from unittest.mock import patch
from findanywhere.schema import main as schema_main, load_schema
from findanywhere.ui.console import main as console_main

def test_schema_file_creation(tmp_path: Path):
    schema_file: Path = tmp_path.joinpath(f'{token_urlsafe(16)}.json')
    with patch('argparse._sys.argv', ['python', 'textfile', 'string_based_evaluation', '--out', str(schema_file)]):
        schema_main()
    assert schema_file.exists()
    assert load_schema(schema_file)


def test_search(tmp_path: Path, data_dir: Path):
    schema_file: Path = tmp_path.joinpath(f'{token_urlsafe(16)}.json')
    out_file: Path = tmp_path.joinpath(f'{token_urlsafe(24)}.json')
    with patch('argparse._sys.argv', ['python', 'textfile', 'string_based_evaluation', '--out', str(schema_file)]):
        schema_main()
    with patch(
            'argparse._sys.argv',
            [
                'python',
                str(schema_file),
                str(data_dir.joinpath('search.json')),
                str(data_dir.joinpath('text.txt')),
                '--out', str(out_file)
            ]
    ):
        console_main()
    assert out_file.exists()
    assert out_file.stat().st_size > 0

