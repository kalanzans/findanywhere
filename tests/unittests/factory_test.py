from collections.abc import Callable
from dataclasses import dataclass
from functools import partial

from findanywhere.types.factory import Config, as_factory


@dataclass(frozen=True)
class SampleConfig(Config):
    times: int = 2


def print_color_n_times(color: str, times: int) -> str:
    return color * times


@as_factory('sample_function', print_color_n_times)
def print_color_n_times_with(config: SampleConfig) -> Callable[[str], str]:
    return partial(print_color_n_times, times=config.times)



def test_get_config_type():
    assert print_color_n_times_with.config_type == SampleConfig


def test_factory_building():
    assert print_color_n_times_with.from_config(SampleConfig(times=5))('blue').count('blue') == 5

