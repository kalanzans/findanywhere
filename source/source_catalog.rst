tabular
~~~~~~~
.. autofunction:: findanywhere.adapters.source.tabular.load_tabular_source




Required Configuration
^^^^^^^^^^^^^^^^^^^^^^
========  ======================
Name      Type
========  ======================
location  <class 'pathlib.Path'>
========  ======================


Optional Configuration
^^^^^^^^^^^^^^^^^^^^^^
=========  ===============
Name       Default
=========  ===============
encoding   utf-8
errors     surrogateescape
delimiter  ,
=========  ===============


textfile
~~~~~~~~
.. autofunction:: findanywhere.adapters.source.text.load_text_source




Required Configuration
^^^^^^^^^^^^^^^^^^^^^^
========  ======================
Name      Type
========  ======================
location  <class 'pathlib.Path'>
========  ======================


Optional Configuration
^^^^^^^^^^^^^^^^^^^^^^
========  ===============
Name      Default
========  ===============
encoding  utf-8
errors    surrogateescape
========  ===============


