string_based_evaluation
~~~~~~~~~~~~~~~~~~~~~~~
.. autofunction:: findanywhere.adapters.evaluation.string_distance.evaluate_by_similarity




Optional Configuration
^^^^^^^^^^^^^^^^^^^^^^
==========  ========================
Name        Default
==========  ========================
similarity  {'name': 'jaro_winkler'}
aggregate   max
==========  ========================


