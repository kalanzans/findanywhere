from collections.abc import Sequence
from functools import partial
from pathlib import Path

from pytest import fixture, mark

from findanywhere.adapters.evaluation.string_distance import evaluate_by_similarity_default
from findanywhere.adapters.source.text import TextPosition
from findanywhere.scores.deduction import average_score
from findanywhere.search.base import search, Search
from findanywhere.search.parallel import parallel_search
from findanywhere.search.sequential import sequential_search
from findanywhere.thresholds.basic import no_threshold
from findanywhere.types.entry import Entry
from findanywhere.types.input_data import InputData


@fixture
def assembled_search() -> Search[TextPosition, str]:
    return partial(search, evaluate_by_similarity_default, average_score, no_threshold)


def test_search(input_data: InputData[str], entries: Sequence[Entry[TextPosition, str]]):
    results = list(search(evaluate_by_similarity_default, average_score, no_threshold, [input_data], entries))
    assert len(results) == 1
    assert results[0].score == 1.0


@mark.parametrize('search_mode', [parallel_search, sequential_search])
def test_parallel_search(
        search_mode,
        input_data: InputData[str],
        entries: Sequence[Entry[TextPosition, str]],
        assembled_search: Search[TextPosition, str],
        tmp_path: Path
):
    result = list(search_mode([input_data], assembled_search, [entries]))
    assert len(result) >= 1
    print(result)



