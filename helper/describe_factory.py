from collections.abc import Sequence
from operator import attrgetter
from pathlib import Path
from typing import Any

from tabulate import tabulate
from toolz import valmap

from findanywhere.adapters.evaluation import evaluate_by_similarity_using, EVALUATION_FACTORIES
from findanywhere.adapters.source import SOURCE_FACTORIES
from findanywhere.scores import DEDUCTION_FACTORIES
from findanywhere.similarity import SIMILARITY_FACTORIES
from findanywhere.thresholds import THRESHOLD_FACTORIES
from findanywhere.types.factory import Factory, FactoryMap


def _add_table_rst(table: dict[str, Any], headers: Sequence[str], name: str) -> str:
    return '\n'.join(
        (
            '',
            name,
            '^'*len(name),
            tabulate(valmap(str, table).items(), headers, 'rst') if table else 'None defined',
            ''
        )
    )



def describe_as_rst(factory: Factory[Any]) -> str:
    factory_function = factory.using
    lines: list[str] = list()
    lines.append(factory.name)
    lines.append('~' * len(factory.name))
    lines.append(f'.. autofunction:: {factory_function.__module__}.{factory_function.__name__}\n\n\n')
    if factory.required:
        lines.append(_add_table_rst(factory.required, ['Name', 'Type'], 'Required Configuration'))
    if factory.defaults:
        lines.append(_add_table_rst(factory.defaults, ['Name', 'Default'], 'Optional Configuration'))
    return '\n'.join(lines)


def describe_factory_map(category: str, factories: FactoryMap) -> None:
    with Path(__file__).parent.parent.joinpath(
            'source', f'{category}_catalog.rst'
    ).open('w', encoding='utf-8') as out:
        for factory in sorted(factories.factories, key=attrgetter('name')):
            out.write(describe_as_rst(factory))
            out.write('\n\n')


CATEGORIES = [
    ('similarity', SIMILARITY_FACTORIES),
    ('threshold', THRESHOLD_FACTORIES),
    ('source', SOURCE_FACTORIES),
    ('evaluation', EVALUATION_FACTORIES),
    ('deduction', DEDUCTION_FACTORIES)
]

if __name__ == '__main__':
    for category, factory_map in CATEGORIES:
        describe_factory_map(category, factory_map)


