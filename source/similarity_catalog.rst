inverted_levenshtein
~~~~~~~~~~~~~~~~~~~~
.. autofunction:: findanywhere.similarity.common.inverted_levenshtein_distance




jaro_winkler
~~~~~~~~~~~~
.. autofunction:: findanywhere.similarity.common.jaro_winkler




token_best_fit_similarity
~~~~~~~~~~~~~~~~~~~~~~~~~
.. autofunction:: findanywhere.similarity.token.token_best_fit_similarity




Optional Configuration
^^^^^^^^^^^^^^^^^^^^^^
=========  =========
Name       Default
=========  =========
delimiter  None
=========  =========


