constant
~~~~~~~~
.. autofunction:: findanywhere.thresholds.basic.constant_threshold




Optional Configuration
^^^^^^^^^^^^^^^^^^^^^^
========  =========
Name        Default
========  =========
constant        0.8
========  =========


no
~~
.. autofunction:: findanywhere.thresholds.basic.no_threshold




