Usage
-----

Installation
~~~~~~~~~~~~

.. _installation:

Write the below command in your terminal to install findanywhere:

.. code-block:: bash

    pip install findanywhere

Installation is typically quick and does not require any additional setup.


Setting Up Schema
~~~~~~~~~~~~~~~~~

.. _usage:

Start by creating a schema for your project. The schema defines the parameters that the tool will use to search and analyze your data. Use the following command to create the schema:

.. code-block:: shell

    findanywhere_schema tabular string_based_evaluation \
    --threshold constant \
    --out schema.yml

After creating the schema, customize it as needed. An example of a schema might look like this:

.. code-block:: yaml

   deduction:
     config: {}
     name: average
   evaluation:
     config:
       aggregate: max
       similarity: token_best_fit_similarity
       similarity_parameter: {}
     name: string_based_evaluation
   source:
     config:
       encoding: utf-8
       errors: surrogateescape
     name: tabular
   threshold:
     config:
       constant: 0.9
     name: constant

Processing Your Data
~~~~~~~~~~~~~~~~~~~~

Once your schema is ready, you can run the tool on your datasets using the following command:

.. code-block:: shell

    findanywhere schema.yml search_data.json input.csv --out result.json_line

After the operation ends, the filtered results will be saved into a new file (`result.json_line`), formatted as json lines.

Running The Tool
~~~~~~~~~~~~~~~~

Use the `--out` option followed by a file name to define where you want the results to be stored. If no output file is specified, the results will be printed out in the console.

For a complete list of options, use the `--help` flag:

.. code-block:: shell

    findanywhere --help