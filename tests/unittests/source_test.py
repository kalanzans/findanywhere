from collections.abc import Sequence
from csv import DictWriter
from functools import partial
from pathlib import Path
from secrets import token_urlsafe

from pytest import fixture, mark

from findanywhere.adapters.source import SOURCE_FACTORIES
from findanywhere.adapters.source.tabular import TablePosition, load_tabular_source, TabularSourceConfig
from findanywhere.adapters.source.text import TextPosition, load_text_source, TextSourceConfig
from findanywhere.ports.source import SourceAdapter
from findanywhere.types.entry import Entry


@fixture
def content(first_name: str, last_name: str) -> list[dict[str, str | int]]:
    return [
        dict(firstname=first_name, last_name=last_name, other=token_urlsafe(32), age=32),
        dict(firstname=token_urlsafe(15), last_name=token_urlsafe(23), occupation=token_urlsafe(64))
    ]


@fixture
def text_file(tmp_path: Path, content: list[dict[str, str | int]]) -> Path:
    filepath: Path = tmp_path.joinpath('text.txt')
    with filepath.open('w') as out:
        for line in content:
            out.write(' '.join(map(str, line.values())))
            out.write('\n')
    return filepath


@fixture
def csv_file(tmp_path: Path, content: list[dict[str, str | int]]) -> Path:
    filepath: Path = tmp_path.joinpath('csv.csv')
    with filepath.open('w', newline='') as out:
        writer: DictWriter = DictWriter(out, {key for entry in content for key in entry})
        writer.writeheader()
        writer.writerows(content)
    return filepath


@mark.parametrize(
    'adapter',
    [
        partial(load_text_source, 'utf-8', 'replace'),
        SOURCE_FACTORIES['textfile'].from_config(TextSourceConfig())
    ]
)
def test_text_file_source(
        adapter: SourceAdapter[Path, TextPosition, str],
        text_file: Path,
        content: list[dict[str, str | int]]
):
    entries: Sequence[Sequence[Entry[TextPosition, str]]] = list(adapter(text_file))
    assert len(entries) == len(content)
    for line_entries, content in zip(entries, content):
        assert len(line_entries) == len(content)



@mark.parametrize(
    'adapter',
    [
        partial(load_tabular_source, 'utf-8', 'replace', ','),
        SOURCE_FACTORIES['tabular'].from_config(TabularSourceConfig())
    ]
)
def test_csv_file_source(
        adapter: SourceAdapter[Path, TablePosition, str],
        csv_file: Path,
        content: list[dict[str, str | int]]
):
    entries: Sequence[Sequence[Entry[TablePosition, str]]] = list(adapter(csv_file))
    assert len(entries) == len(content)


def test_csv_file_source_with_extra_column(
    data_dir: Path
):
    adapter = SOURCE_FACTORIES['tabular'].from_config(TabularSourceConfig())
    entries: Sequence[Sequence[Entry[TablePosition, str]]] = list(adapter(data_dir.joinpath('csv_with_extra_columns.csv')))
    assert max(map(len, entries)) == 4