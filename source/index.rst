.. findanywhere documentation master file, created by
   sphinx-quickstart on Wed May 15 22:26:50 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to FindAnywhere's documentation!
=====================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   introduction.rst
   user_guide.rst
   sources.rst
   evaluations.rst
   deduction.rst
   threshold.rst
   similarities.rst




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
