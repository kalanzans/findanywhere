from collections.abc import Sequence
from pathlib import Path
from secrets import token_urlsafe

from pytest import fixture

from findanywhere.adapters.source.text import TextPosition
from findanywhere.ports.evaluation import InputData
from findanywhere.ports.source import Entry

@fixture
def first_name() -> str: return 'Alice'

@fixture
def last_name() -> str: return 'Ashcroft'


@fixture
def input_data(first_name: str, last_name: str) -> InputData[str]:
    return InputData.parse(dict(firstname=first_name, lastname=last_name))

@fixture
def entries(first_name: str, last_name: str) -> Sequence[Entry[TextPosition, str]]:
    return (
        Entry(TextPosition(0, 1), first_name),
        Entry(TextPosition(0, 2), token_urlsafe(64)),
        Entry(TextPosition(0, 3), last_name)
    )


@fixture
def data_dir() -> Path:
    return Path(__file__).parent.joinpath('data')

