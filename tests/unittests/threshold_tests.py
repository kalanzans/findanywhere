from operator import attrgetter

from pytest import mark

from findanywhere.thresholds import THRESHOLD_FACTORIES
from findanywhere.types.factory import Factory
from findanywhere.types.input_data import InputID
from findanywhere.types.similarity import ThresholdFilter, ScoredEvaluation


@mark.parametrize('threshold_factory', THRESHOLD_FACTORIES.factories, ids=attrgetter('name'))
def test_threshold(threshold_factory: Factory[ThresholdFilter]):
    assert threshold_factory.from_config(dict())(ScoredEvaluation(InputID('ABC'), dict(), 1.0))