from operator import attrgetter

from pytest import mark

from findanywhere.similarity import SIMILARITY_FACTORIES
from findanywhere.types.factory import Factory, Config
from findanywhere.types.similarity import Similarity


@mark.parametrize('similarity_factory', SIMILARITY_FACTORIES.factories, ids=attrgetter('name'))
def test_similarity(similarity_factory: Factory[Similarity[str], Config]):
    similarity: Similarity[str] = similarity_factory.from_config(similarity_factory.config_type())
    assert similarity('Hello', 'Hello') > similarity('Hello', 'Holla')


